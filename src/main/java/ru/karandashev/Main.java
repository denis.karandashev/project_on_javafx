package ru.karandashev;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 * @author Denis Karandashev
 * @project Project_on_JavaFX
 * @date 10.12.2020
 */
public class Main extends Application {

  public static void main(String[] args) {
    launch();
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    // 1. создали элемент-метку с текстом
    Label label = new Label("First example JavaFX");
    // 2. корневой элемент, который умеет располагать элементы по горизонтали
    //    и задаю расположение по центру
    HBox box = new HBox();
    box.setAlignment(Pos.CENTER);
    // 3. добавляем элемент к корневому элементу, который нужно будет рахместить на сцене, которую
    //    нужно будет разместить на подмостках - primaryStage
    box.getChildren().add(label);
    // 4. создаем сцену, задаем ширину и высоту
    Scene scene = new Scene(box, 400, 400);
    // 5. помещаем сцену на подмостки
    primaryStage.setScene(scene);
    // 6. делаем подмостки видимыми
    primaryStage.show();
  }
}
